package ss.Player;

import ss.Board.Card;
import ss.Board.Deck;


import java.util.List;

public class Player {
    private List<Card> hand;
    private String name;

    public Player(String name) {
        this.name = name;
    }


    public String toString() {
        return "ss.Player.Player{" +
                "hand=" + hand +
                '}';
    }

    public String getName() {
        return name;
    }

    public String setName(String newName) {
        return this.name = newName;
    }


    public List<Card> getHand() {
        return hand;
    }


    public Card drawCard(Deck deck) {
        return deck.drawCard();
    }


    public Card chooseCard(int position) {
        return null;
    }


    public Card playCard(Card card) {
        return null;
    }


    public void setHand(List<Card> hand) {
        this.hand = hand;
    }


    public Card CardinRotation() {
        return null;
    }


    public void callUno() {

    }
}