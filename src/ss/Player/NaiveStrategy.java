package ss.Player;

import static ss.Player.ComputerPlayer.*;

import ss.Board.Card;
import ss.Game;
import ss.Board.Deck;

public class NaiveStrategy {

    /**
     * This strategy is quite simple, all it does is iterate through every card in the hand.
     * It checks if the current card in iteration is valid to play, and if so it plays the card.
     */


    public static void play() {
        boolean cardCanBePlayed = false;

        for (int i = 0; i < hand.size(); i++) {
            // if the color of hand.get(i) == topcard.color || value of hand.get(i) == topcard.value, then the color is valid
            if (hand.get(i).getColor() == (Game.playPile.get(Game.playPile.size()-1).getColor())) {
                Game.playPile.add(hand.get(i));
                hand.remove(i);
                cardCanBePlayed = true;
                break;
            }
            else if (hand.get(i).getValue() == (Game.playPile.get(Game.playPile.size()-1).getValue())) {
                Game.playPile.add(hand.get(i));
                hand.remove(i);
                cardCanBePlayed = true;
                break;
            }
        }
        if (!cardCanBePlayed) {
            Card drawncard = Deck.drawCard();
            hand.add(drawncard);
        }


    }
}
