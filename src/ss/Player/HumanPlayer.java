package ss.Player;

import ss.Board.Card;
import ss.Game;
import ss.TextIO;

import java.util.ArrayList;
import java.util.List;

public class HumanPlayer extends Player {

    private List<Card> hand;
    private String name;
    private boolean hasCalledUno;
    private int score;

    // Player.HumanPlayer constructor
    public HumanPlayer(String name) {
        super(name);
        this.name = name;
        this.hand = new ArrayList<>();
        this.score = score;
        this.hasCalledUno = false;
    }


    public List<Card> getHand() {
        return hand;
    }


    public String getName() {
        return name;
    }



    public void drawCard(Card card) {
        hand.add(card);
    }


    public Card chooseCard(int choice) {
        // first get the hand of the player
        // ask the player to choose an index in the list
        choice = (TextIO.getInt() - 1);
        Card card = hand.get(choice);
        return card;
    }


    public void setHand(List<Card> hand) {
        this.hand = hand;
    }


    public Card CardinRotation() {
        // returns the top card in the playpile
        // this card dictates the flow of the game
        return Game.playPile.get(Game.playPile.size() - 1);
    }


    public String toString() {
        return "Player.HumanPlayer{" + "hand=" + hand + '}';
    }


    public boolean canCallUno() {
        return hand.size() == 1 && !hasCalledUno;
    }


    public void callUno() {
        if (canCallUno()) {
            hasCalledUno = true;
        } else {
            throw new IllegalStateException("Cannot call UNO");
        }
    }


    public int playerScore() {
        // while loop that iterates through the player's hand and checks the value of the card
        // adds the value for each card into the score variable
        // this method should be called after every round
        for (int i = 0; i < hand.size(); i++) {
            Card card = hand.get(i);
            score = score + card.getCardValue(card);
        }
        return score;
    }

}
    // Checklist:
    // player's hand of cards array
    // ability to draw a card
    // ability to place a card onto drawpile
    // see their current hand
    // see the current card in playpile rotation
    // player needs to call uno when 1 card is left (create function called draw 4 in case uno is not called on time




