package ss.Player;

import ss.Board.Card;

import java.util.ArrayList;
import java.util.List;

public class ComputerPlayer extends Player{

    public static List<Card> hand;
    private final NaiveStrategy strategy;
    private String name;
    private static int score;
    private boolean hasCalledUno;

    // Player.ComputerPlayer constructor
    public ComputerPlayer(String strategy) {
        super(strategy);
        name = "CPU";
        this.hand = new ArrayList<>();
        this.score = score;
        this.strategy = new NaiveStrategy();
        this.hasCalledUno = false;
    }


    public List<Card> getHand() {
        return hand;
    }


    public String getName() {
        return name;
    }


    public void drawCard(Card card) {
            hand.add(card);
    }


    public Card chooseCard(int position) {
        return null;
    }


    public void setHand(List<Card> hand) {

    }


    public Card CardinRotation() {

        return null;
    }


    public boolean canCallUno() {
        return hand.size() == 1 && !hasCalledUno;
    }


    public void callUno() {
        if (canCallUno()) {
            hasCalledUno = true;
        } else {
            throw new IllegalStateException("Cannot call UNO");
        }
    }


    public int playerScore() {
        // while loop that iterates through the player's hand and checks the value of the card
        // adds the value for each card into the score variable
        // this method should be called after every round
        for (int i = 0; i < hand.size(); i++) {
            Card card = hand.get(i);
            score = score + card.getCardValue(card);
        }
        return score;
    }


}
