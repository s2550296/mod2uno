package ss;

import ss.Board.Card;
import ss.Board.Deck;
import ss.Player.Player;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game {

    private static final int NUM_OF_PLAYERS = 2;

    private static String name;

    private static int noOfPlayers;
    private final List<Player> listOfPlayers;

    private static Deck deck;

    private Card initial;

    private static Player[] players;

    public static List<Card> playPile;

    private Card.Color validColor;
    private Card.Value validValue;

    boolean direction;

    public Game(List<Player> listOfPlayers) {
        this.listOfPlayers = listOfPlayers;
        deck = new Deck();
        deck.reset();
        deck.shuffle();
        playPile = new ArrayList<>();


        direction = false; // false: anti-clockwise
        // true: clockwise

        for (int i = 0; true; i++) {
    //       if (listOfPlayers.size() >= 2) {
            System.out.println("Do you wish to add more players? Yes or No?");
            String response = TextIO.getlnString();
             if (response.equals("No")) {
                break;
            }
             System.out.println("Type your name: ");
             String name = TextIO.getlnString();
             ss.Player.Player player = new ss.Player.Player(name);
             player.setName(name);
             listOfPlayers.add(player);
        }
        players = new Player[listOfPlayers.size()];
        for(int i = 0; i<listOfPlayers.size(); i++) {
            players[i] = new Player(name);
            players[i].setHand(Arrays.asList(deck.drawMultiple(2)));
        }
    }

//    public void switchTurn(){
//        if(currentPlayer == 0){
//            currentPlayer = 1;
//        } else {
//            currentPlayer = 0;
//        }
//    }

    public void start() {
        deck.shuffle();
        //initial card picked from the deck to start the play
        initial = Deck.drawCard();
        validColor = initial.getColor();
        validValue = initial.getValue();


        // if game starts with a WILD or a WILD+4, then it is placed back into the deck and then shuffled,
        //and a new initial card is picked.
//        boolean validInitialCard = false;
//        while (!validInitialCard) {
//            if (initial.getValue() == ss.Board.Card.Value.Wild || initial.getValue() == ss.Board.Card.Value.Wild_Four) {
//                deck.add(initial);
//                deck.shuffle();
//                initial = deck.drawCard();
//            } else {
//
//                validInitialCard = true;
//            }
//        }

        if (initial.getValue() == Card.Value.Wild || initial.getValue() == Card.Value.Wild_Four) {
            deck.add(initial);
            deck.shuffle();
            initial = Deck.drawCard();
        }

        // initial card is a draw two
//        if (initial.getValue() == ss.Board.Card.Value.DrawTwo) {
//            players[listOfPlayers.size()]
//            players[listOfPlayers.size()].getHand().addAll(Arrays.asList(deck.drawMultiple(2)));
//        }

        // initial card is a skip
//        if (initial.getValue() == ss.Board.Card.Value.Skip) {
//            if (direction) {
//                currentPlayer = players[getClockNext()];
//            } else {
//                currentPlayer = players[getAntiClockNext()];
//            }
//        }
//
//        // initial card is a reverse, direction is changed
//        if (initial.getValue() == ss.Board.Card.Value.Reverse) {
//            direction = true;
//        }
        playPile.add(initial);
        System.out.println("initial card: " + initial);

        boolean writeTest = false;
        while(!writeTest) {
            System.out.println("ss.Player.Player 1 start your turn: " + listOfPlayers.get(0).getName());



            if ((!(players[listOfPlayers.size()-1].getHand().size()==0))) {
                execute(players[listOfPlayers.size()-1]);
                execute(players[listOfPlayers.size()-1]);
            } else {
                writeTest = true;
            }
        }
        //check for this card in player deck
    }
    //
    private void execute(Player player){
        System.out.println("ss.Player.Player started execution "+ player.getName());
        if((getExistingCardColour(player.getHand()) != null)) {
            Card chosenCard = getExistingCardColour(player.getHand());
            System.out.println("color matched = " + chosenCard);
            playPile.add(chosenCard);
            initial = chosenCard;
            System.out.println(" new initial = " + initial);


        } else if (getExistingCardValue(player.getHand()) != null) {
            Card chosenCard =getExistingCardValue(player.getHand());
            System.out.println("value matched = "+chosenCard);
            playPile.add(chosenCard);
            initial = chosenCard;
            System.out.println(" new initial = "+initial);

        } else {
            System.out.println("nothing matched, picking up from deck");

            Card c = Deck.drawCard();
            System.out.println("card from deck " + c);
            player.getHand().add(c);
            System.out.println("updated current player's hand: " + player.getHand());
            if (c.getColor() == initial.getColor() || c.getValue() == initial.getValue()) {
                playPile.add(c);
            }
        }
    }

    private Card getExistingCardColour(List<Card> playerhand){
        // get the player and his hand
        return playerhand.stream()
                .filter(card -> card.getColor().equals(initial.getColor()))
                .findAny().orElse(null);
    }

    private Card getExistingCardValue(List<Card> playerhand){
        // get the player and his hand
        return playerhand.stream()
                .filter(card -> card.getValue().equals(initial.getValue()))
                .findAny().orElse(null);
    }

    public boolean emptyHand(Player player) {
        if (player.getHand().size() == 0) {
            return true;
        }
        return false;
    }


    public Card getTopCard() {
        return new Card(validColor, validValue);
    }

    public int getClockNext() {
        for (int i = 0; i < players.length; i++) {
            if (players[noOfPlayers] == players[i]) {
                return i + 1;
            }
        }
        return 0;
    }

    public int getAntiClockNext() {
        for (int i = 0; i < players.length; i++) {
            if (players[noOfPlayers] == players[0]) {
                return players.length - 1;
            }
            if (players[noOfPlayers] == players[i]) {
                return i - 1;
            }
        }
        return 0;
    }


    public boolean gameOver() {
        for (int i = 0; i < players.length; i++) {
            if (emptyHand(players[i])) {
                return true;
            }
        }
        return false;
    }



//    public ss.Board.Card getPlayerCard(HumanPlayer player, int choice) {
//        List<ss.Board.Card> hand = player.getHand();
//        return playerHand.get(choice);
//    }

    public boolean validCardPlay(Card card) {
        return card.getColor() == validColor || card.getValue() == validValue;
    }

    public void checkPlayerTurn(String pNum) {
        for (int i = 0; i < players.length; i++) {
            if (players[noOfPlayers] != players[i]) {
                System.out.println("It is not " + pNum + " 's turn");
            }
        }
    }

    public void replaceDeckWithPlayPile(String pNum) {
        checkPlayerTurn(pNum);
        if (Deck.isEmptyDeck()) {
            deck.replaceDeckWith(playPile);
            deck.shuffle();
        }
    }

    public void setCardColor(Card.Color color) {
        validColor = color;
    }

    public void printStatus(){
        for (int i = 0; i < players.length; i++) {
            System.out.println(players[i].getHand());
        }
    }

    public List<Card> setPlayerHand(int pNum, String name) {
        players[pNum] = new Player(name);
        players[pNum].setHand(Arrays.asList(deck.drawMultiple(10)));
//        System.out.println(name);
        return players[pNum].getHand();
    }


    public static void main(String[] args) {
        List<Player> str = new ArrayList<>();

        Game game = new Game(str);
        game.start();

    }
}