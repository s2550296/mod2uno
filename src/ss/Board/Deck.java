package ss.Board;

import java.util.*;


public class Deck {

    // implementations:
    // populate the deck CHECK
    // shuffle deck CHECK
    // distribute cards from deck
    // pick up cards from deck
    // start game with random card
    // keep track of distributed cards
    // reset the deck CHECK
    // replace drawpile with playpile

    public Card[] getCards() {
        return cards;
    }

    public void setCards(Card[] cards) {
        this.cards = cards;
    }

    private static Card[] cards;
    private static int cardsInDeck;

    public Deck() {
        cards = new Card[108];

        Card.Color[] colors = Card.Color.values();
        cardsInDeck = 0;

        for (int i = 0; i < colors.length - 1; i++) {
            Card.Color color = colors[i];
            cards[cardsInDeck++] = new Card(color, Card.Value.getValue(i));

            for (int j = 1; j < 10; j++) {
                cards[cardsInDeck++] = new Card(color, Card.Value.getValue(j));
                cards[cardsInDeck++] = new Card(color, Card.Value.getValue(j));
            }

            Card.Value[] values = new Card.Value[]{Card.Value.DrawTwo, Card.Value.Skip, Card.Value.Reverse};

            for (Card.Value value : values) {
                cards[cardsInDeck++] = new Card(color, value);
                cards[cardsInDeck++] = new Card(color, value);
            }
        }

        Card.Value[] values = new Card.Value[]{Card.Value.Wild, Card.Value.Wild_Four};
        for (Card.Value value : values) {
            for (int i = 0; i < 4; i++) {
                cards[cardsInDeck++] = new Card(Card.Color.Wild, value);
            }
        }
    }

    public void reset() {
        Card.Color[] colors = Card.Color.values();
        cardsInDeck = 0;

        for (int i = 0; i < colors.length - 1; i++) {
            Card.Color color = colors[i];
            cards[cardsInDeck++] = new Card(color, Card.Value.getValue(0));

            for (int j = 1; j < 10; j++) {
                cards[cardsInDeck++] = new Card(color, Card.Value.getValue(j));
                cards[cardsInDeck++] = new Card(color, Card.Value.getValue(j));
            }

            Card.Value[] values = new Card.Value[]{Card.Value.DrawTwo, Card.Value.Skip, Card.Value.Reverse};

            for (Card.Value value : values) {
                cards[cardsInDeck++] = new Card(color, value);
                cards[cardsInDeck++] = new Card(color, value);
            }
        }

        Card.Value[] values = new Card.Value[]{Card.Value.Wild, Card.Value.Wild_Four};
        for (Card.Value value : values) {
            for (int i = 0; i < 4; i++) {
                cards[cardsInDeck++] = new Card(Card.Color.Wild, value);
            }
        }
    }

    public void replaceDeckWith(List<Card> cards) {
        this.cards = cards.toArray(new Card[cards.size()]);
        this.cardsInDeck = this.cards.length;
    }

    public static boolean isEmptyDeck() {
        return cardsInDeck == 0;
    }

    //iterate through a loop, swap the current index element with the randomly generated index element

    public void shuffle() {
        Random rand = new Random();

        for (int i = 0; i < cards.length; i++) {
            int[] temparray = new int[cards.length];
            int swapindex = rand.nextInt(cards.length);
            int sudden = temparray[swapindex];
            temparray[swapindex] = temparray[i];
            temparray[i] = sudden;
        }
    }

    public static Card drawCard() {
        if (isEmptyDeck()) {
            System.out.println("Board.Deck is empty, cannot pick up a card");
            return null;
        } else {
            Random rand = new Random();
            int randomIndex = rand.nextInt(cardsInDeck) + 1;
            return cards[randomIndex];
        }
    }
    //
    //


    public Card[] drawMultiple(int i) {
        if (i < 0) {
            System.out.println("Can't draw negative cards");
        }

        if (i > cardsInDeck) {
            System.out.println("Can't draw more than deck");
        }

        Card[] mulCardArray = new Card[i];

        for (int n = 0; n < i; n++) {
            Random rand = new Random();

            mulCardArray[n] = drawCard();
        }
        return mulCardArray;
    }

    public void add(Card card) {
        cards[cards.length-1] = card;
        cardsInDeck++;
    }


    // RAHUL 14/12/2022
//    public Board.Card assigncard(){
//        var tempcard = drawCard(Board.Card.Board.Card);
//
//    }



}

