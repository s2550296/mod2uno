package ss.Board;

public class Card {


    public enum Color {
        Red, Blue, Green, Yellow, Wild;
        private static final Color[] colors = Color.values();

    }

    public enum Value {
        Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Skip, Reverse, DrawTwo, Wild, Wild_Four;
        private static final Value[] values = Value.values();

        public static Value getValue(int i) {
            return Value.values[i];
        }
    }

    private final Color color;
    private final Value value;

    public Card(Color color, Value value) {
        this.color = color;
        this.value = value;
    }

    public Color getColor() {
        return this.color;
    }

    public Value getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Board.Card{" +
                "color=" + color +
                ", value=" + value +
                '}';
    }

    public int getCardValue(Card card) {
        switch (this.value) {
            case Zero:
                return 0;
            case One:
                return 1;
            case Two:
                return 2;
            case Three:
                return 3;
            case Four:
                return 4;
            case Five:
                return 5;
            case Six:
                return 6;
            case Seven:
                return 7;
            case Eight:
                return 8;
            case Nine:
                return 9;
            case DrawTwo:
            case Reverse:
            case Skip:
                return 20;
            case Wild:
            case Wild_Four:
                return 50;
            default:
                return 0;
        }
    }


}
