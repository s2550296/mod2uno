package ss.Test;

import org.testng.annotations.Test;
import ss.Board.Card;
import ss.Player.HumanPlayer;

import static org.testng.Assert.assertEquals;


public class HumanPlayerTest {

    @Test
    public void testCalculateScore() {
        // create a new player with a hand of cards
        HumanPlayer player = new HumanPlayer("Rahul");
        // add 4 cards and check to see if the values add up
        player.drawCard(new Card(Card.Color.Blue, Card.Value.DrawTwo));
        player.drawCard(new Card(Card.Color.Green, Card.Value.Reverse));
        player.drawCard(new Card(Card.Color.Red, Card.Value.Zero));
        player.drawCard(new Card(Card.Color.Yellow, Card.Value.Wild_Four));

        // calculate the player's score using the playerScore method
        int score = player.playerScore();

        // assert that the score is correct
        assertEquals(score,90);
    }


    @Test
    public void TestCanCallUno() {

        HumanPlayer player = new HumanPlayer("Rahul");

        // draw 3 cards to the hand
        player.drawCard(new Card(Card.Color.Blue, Card.Value.DrawTwo));
        player.drawCard(new Card(Card.Color.Red, Card.Value.Zero));
        player.drawCard(new Card(Card.Color.Yellow, Card.Value.Wild_Four));


        boolean callUno = player.canCallUno();

        assertEquals(callUno, false);


    }


}
